création webapp :
	- En ligne de commande (ftp ou non)
				- création app service : az appservice plan create config:( groupe de ressource,nom, linux ou windows, SKU)
				
				- création webapp : az create webapp config:(groupe de ressource, nom app-plan, nom webapp, runtime)
				
				- config webapp : variable d'environnement, config webapp(SCM_DO_BUILD_DURING_DEPLOYMENT,WEBSITE_RUN_FROM_PACKAGE)
				
				- Startup Command : az webapp config set .... -startup-file "python -m uvicorn app:app --host 0.0.0.0"
				
				- Terraform
				
				- one drive/DropBox
				
				- Azure ARM
				
				Deployment manuel :
					- External repos Github/Gitlab/Azure Devops
					- One drive/ Dropbox
					- ZIP : az webapp deploy --type zip
				
				Deployment continue (CI/CD): 
				
					- Github/Bitbucket :
					  az webapp deployment source config \
					  	-n $appName\
						-g $ResourceGroup \
						--repo-url $SourcePath \
						--branch prod
	    						
	    				- Repos local : 
	    				az webapp create --resource-group <group-name>
					  --plan <plan-name>
					  --name <app-name>
				          --runtime "<runtime-flag>" 
					  --deployment-local-git
	    						