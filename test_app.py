import requests
import sys
import argparse


def test_hello(url):
    try:
        response = requests.get(url)
        status = response.status_code
        data = response.content
        if status == 200:
            print("Your app is running !", status)
            if data == b'Hello Simplon':
                print("Your data is correct !", data)
                sys.exit(0)
            else:
                print("Your data is not correct !", data)
                sys.exit(1)
    except requests.ConnectionError as e:
        print("Connection error, your app is not running !")
        print("This is more details :", e)
        sys.exit(1)
    


def test_apprenant(url):
    res = b'[{"Avatar":"ma_photo.jpg","C1":"2","C10":"0","C11":"1","C12":"0","C13":"0","C14":"0","C15":"0","C16":"0","C17":"0","C2":"2","C3":"1","C4":"0","C5":"1","C6":"1","C7":"0","C8":"0","C9":"0","Mail":"yoan30470@hotmail.fr","Nom":"Sanchez","Prenom":"Yoan"}]\n'
    try: 
        response = requests.get(url +
        '/competences/Yoan')
        data = response.content
        status = response.status_code
        headers = "application/json"
        if status == 200:
            print("Your route is ok !", status)
            if data == res :
                print("Your data is correct !")
                if response.headers.get('Content-Type') == headers:
                    print("Your data format is correct !")
                    sys.exit(0)
                else:
                    print("Your data format is not correct !")
                    sys.exit(1)
            else:
                print("Your data is not correct !", data)
                sys.exit(1)
    except requests.ConnectionError as e :
        print("Connection error, your app is not running")
        print("This is more details :", e)
        sys.exit(1)


def test_not_defined(url):
    try:
        response = requests.get(url + '/false')
        status = response.status_code
        if status == 404:
            print("Your app is running but the route is not exist.")
            sys.exit(0)
        else:
            print("Your app is running, but you must check your configuration")
            sys.exit(1)
    except requests.ConnectionError as e :
        print("Connection error, your app is not running")
        print("This is more details :", e)
        sys.exit(1)


def test_apprenant_not_found(url):
    try:
        response = requests.get(url + '/competences/Marcel')
        status = response.status_code
        content = response.content
        if status == 200:
            print("Your route is ok")
            if content ==  b'"apprenant not found"\n':
                print("Your data is correct !")
                sys.exit(0)
            else:
                print("Your app is running, but you must check your configuration")
                sys.exit(1)
    except requests.ConnectionError as e:
        print("Connection error, your app is not running")
        print("This is more details :", e)
        sys.exit(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-test_hello',
                type = test_hello,
                required=True,
                action = "store",
                help=""" Test the home of app. Don't forget the url of app exemple : \n python test_app.py -test_hello http://localhost:5000 """)

    parser.add_argument('-test_apprenant', 
                type = test_apprenant,
                required=True,
                action = "store",
                help="""Test /apprenant/<name> of the app. Don't forget the url of app exemple : \n python test_app.py -test_apprenant http://localhost:5000  """)

    parser.add_argument('-test_apprenant_not_found',
                type = test_apprenant_not_found,
                required=True,
                action = "store",
                help="""Test /apprenant/<name> with name does not exist in database. Don't forget the url of app exemple : \n python test_app.py -test_apprenant_not_found http://localhost:5000 """)

    parser.add_argument('-test_not_defined',
                type = test_not_defined,
                required=True,
                action = "store",
                help="""Test a false route. Don't forget the url of app exemple :  \n python test_app.py -test_not_defined http://localhost:5000 """)

    args = parser.parse_args()

