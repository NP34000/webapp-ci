import sqlite3
import sys

def change_competence(cursor):
    prenom = input("Tapez votre prénom pour pouvoir changer vos compétences :")
    
    competences = ["Compétence Cloud",
                  "Compétence DevOps"]

    while True:
        [print(i, competence) for i, competence in enumerate(competences,1)]
        print("3 Sortir du programme")
        choix = input("Choissisez les compétences que vous souhaitez modifié :")
        try:
            choix = int(choix)
        except ValueError:
            print("Vous n'avez pas saisie une bonne valeur ")
            continue
        if choix == len(competences) + 1:
            break
        elif choix < 1 : 
            print("Vous n'avez pas saisie une bonne valeur ")
            continue
        try:
            print(f"Vous allez modifié les {competences[choix -1]}")
            break
        except IndexError:
            print("Vous n'avez pas saisie une bonne valeur !")

    if choix == 1:
        c1_cloud = input(f' C1 {competences[0]}: ')
        c2_cloud = input(f' C2 {competences[0]}: ')
        c3_cloud = input(f' C3 {competences[0]}: ')
        c4_cloud = input(f' C4 {competences[0]}: ')
        c5_cloud = input(f' C5 {competences[0]}: ')
        c6_cloud = input(f' C6 {competences[0]}: ')
        c7_cloud = input(f' C7 {competences[0]}: ')
        c8_cloud = input(f' C8 {competences[0]}: ')
        c9_cloud = input(f' C9 {competences[0]}: ')
        c_cloud = c1_cloud,c2_cloud,c3_cloud,c4_cloud,c5_cloud,c6_cloud,c7_cloud,c8_cloud,c9_cloud, prenom
        sql ="""UPDATE Apprenants SET C1=?, C2=?,C3=?,C4=?,C5=?,C6=?,C7=?,C8=?,C9=? WHERE Prenom=?"""
        cursor.execute(sql,c_cloud)
        print(f"BRAVO {prenom} tes compétences Cloud ont été mis a jour ! ")


    if choix == 2:
        c1_devops = input(f' C1 {competences[1]}: ')
        c2_devops = input(f' C2 {competences[1]}: ')
        c3_devops = input(f' C3 {competences[1]}: ')
        c4_devops = input(f' C4 {competences[1]}: ')
        c5_devops = input(f' C5 {competences[1]}: ')
        c6_devops = input(f' C6 {competences[1]}: ')
        c7_devops = input(f' C7 {competences[1]}: ')
        c8_devops = input(f' C8 {competences[1]}: ')
        c_devops = c1_devops, c2_devops,c3_devops,c4_devops,c5_devops,c6_devops,c7_devops,c8_devops, prenom
        sql ="""UPDATE Apprenants SET C10=?, C11=?,C12=?,C13=?,C14=?,C15=?,C16=?,C17=? WHERE Prenom=?"""
        cursor.execute(sql,c_devops)
        print(f"BRAVO {prenom} tes compétences DevOPS ont été mis a jour !")



def main():
    conn = sqlite3.connect('promo_simplon.db')
    c = conn.cursor()
    change_competence(c)
    conn.commit()


if __name__ == "__main__":
    sys.exit(main())