FROM python:3.8.6

RUN apt-get update
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

RUN py.test
EXPOSE 5000
CMD ["python", "app.py"]