var ctx = document.getElementById('myChart').getContext('2d');
var prenom = document.getElementById('prenom');
var nom = document.getElementById('nom');
var mail = document.getElementById('mail');
const btnuser = document.getElementsByClassName('btn btn-outline-secondary');
// console.log(btnuser)

var chart = new Chart(ctx, {
  type: 'bar',
  data: {
      labels: [
          'Développer des programmes et scripts simples',
          'Utiliser les machines virtuelles (serveurs web, de base de données…)',
          'Déployer une application sur le cloud',
          'Développer une application cloud native',
          'Optimiser une application cloud native',
          'Analyser une infrastructure réseau simple',
          'Développer une application simple pour le cloud en exploitant les outils de développement de la plateforme (PaaS et SaaS)',
          'Analyser un besoin en développement d’application cloud',
          'Sécuriser une applications à tous les niveaux en environnement cloud'
      ],
      legend:[
        "C1",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "C8",
        "C9",
      ],

      datasets: [{
          label: 'Développeur Cloud',
          backgroundColor: 'rgb(206, 0, 51)',
          borderColor: 'rgb(206, 0, 51)',
          data: [0,1,2,2,2,3,3,2,1]
      }]
  },

  // Configuration options go here
  options: {
    maintainAspectRatio: false,
    responsive:true,
      legend: {
          display: true},
          tooltips: {
            enable:true
          },
    scales: {
    yAxes:[{
      ticks: {
        beginAtZero: true,
      steps: 1,
      stepValue: 1,
       max: 3
    }}],
      xAxes: [{display: false}],
    }}
        });



var competences = []

function addData(chart, data) {
  // chart.data.labels.push(label);
  chart.data.datasets.forEach((dataset) => {
      console.log(dataset.data)
      dataset.data = data
      console.log(dataset.data)
  });
  chart.update();
  console.log("add")
}


function removeData(chart) {
  // chart.data.labels.pop();
  console.log(chart.data)
  chart.data.datasets.forEach((dataset) => {
      console.log(dataset.data)
      dataset.data = [0,0,0,0,0,0,0,0,0]
      //dataset.data.pop();
      console.log(dataset.data)
  });
  chart.update();
  console.log("suppr")
}

const selectuser = selecteduser => {
    fetch("http://127.0.0.1:5000/"+selecteduser)
    .then(
      function(response) {
        if (response.status !== 200) {
          console.log('Looks like there was a problem. Status Code: ' +
            response.status);
          return;
        }
        // Examine the text in the response
        response.json().then(function(data) {
          console.log('data recupérée : ', data[0])
          competences = []
          competences.push(parseInt(data[0]["C1"]));
          competences.push(parseInt(data[0]["C2"]));
          competences.push(parseInt(data[0]["C3"]));
          competences.push(parseInt(data[0]["C4"]));
          competences.push(parseInt(data[0]["C5"]));
          competences.push(parseInt(data[0]["C6"]));
          competences.push(parseInt(data[0]["C7"]));
          competences.push(parseInt(data[0]["C8"]));
          competences.push(parseInt(data[0]["C9"]));
          console.log(competences)
          removeData(chart)
          addData(chart,competences)
          prenom.innerHTML = data[0]["Prenom"]
          nom.innerHTML = data[0]["Nom"]
          mail.innerHTML = data[0]["Mail"]
        });
      }
    )
    .catch(function(err) {
      console.log('Fetch Error :-S', err);
    });}
  


for (i = 0 ;i< btnuser.length; i++){
  btnuser[i].addEventListener('click', e => {
    selectuser(e.target.value)
    // console.log(e.target.value)
  }
  );
  
}



